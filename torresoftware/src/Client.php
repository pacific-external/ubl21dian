<?php
namespace ubl21dian;

use DOMDocument;
use Exception;
use GuzzleHttp\Exception\TransferException;
use Storage;
use ubl21dian\Templates\Template;

/**
 * Client.
 */
class Client
{
    /**
     * Curl.
     *
     * @var object
     */
    private $curl;

    /**
     * to.
     *
     * @var string
     */
    private $to;

    /**
     * Response.
     *
     * @var string
     */
    private $response;

    /**
     * Data sent
     *
     * @var string
     */
    private $data;

    /**
     * Construct.
     *
     * @param \Stenfrank\UBL21dian\Templates\Template $template
     */
    public function __construct(
        Template $template,
        $GuardarEn = false
    ) {
        $this->curl = new \GuzzleHttp\Client([
            'connect_timeout' => 180,
            'timeout' => 180,
            'headers' => [
                'Accept' => 'application/xml',
                'Content-Type' => 'application/soap+xml',
                'Content-Length' => strlen($template->xml),
            ],
        ]);

        $this->to = $template->To;
        $this->data = $template->xml;

        Storage::put(preg_replace("/[\r\n|\n|\r|\s]+/", "", $GuardarEn), $template->xml);

        $this->exec();

        return $this;
    }

    /**
     * Exec.
     */
    private function exec()
    {
        try {
            $this->response = $this->curl->post($this->to, ['body' => $this->data])->getBody()->getContents();
        } catch (TransferException $e) {
            throw new Exception('Class ' . get_class($this) . ': ' . $e->getMessage());
        }
    }

    /**
     * Get response.
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Get response to object.
     *
     * @return object
     */
    public function getResponseToObject($GuardarEn = false)
    {
        try {
            $xmlResponse = new DOMDocument();
            $xmlResponse->loadXML($this->response);
            $GuardarEn = preg_replace("/[\r\n|\n|\r]+/", "", $GuardarEn);

            if ($GuardarEn) {
                Storage::put(preg_replace("/[\r\n|\n|\r|\s]+/", "", $GuardarEn), $this->response);
            }

            return $this->xmlToObject($xmlResponse);

        } catch (\Exception $e) {
            throw new Exception('Class ' . get_class($this) . ': ' . $this->to . ' ' . $this->response);
        }
    }

    /**
     * XML to object.
     *
     * @param mixed $root
     *
     * @return mixed
     */
    protected function xmlToObject($root)
    {
        $regex = '/.:/';
        $dataXML = [];

        if ($root->hasAttributes()) {
            $attrs = $root->attributes;

            foreach ($attrs as $attr) {
                $dataXML['_attributes'][$attr->name] = $attr->value;
            }
        }

        if ($root->hasChildNodes()) {
            $children = $root->childNodes;

            if (1 == $children->length) {
                $child = $children->item(0);

                if (XML_TEXT_NODE == $child->nodeType) {
                    $dataXML['_value'] = $child->nodeValue;

                    return 1 == count($dataXML) ? $dataXML['_value'] : $dataXML;
                }
            }

            $groups = [];

            foreach ($children as $child) {
                if (!isset($dataXML[preg_replace($regex, '', $child->nodeName)])) {
                    $dataXML[preg_replace($regex, '', $child->nodeName)] = $this->xmlToObject($child);
                } else {
                    if (!isset($groups[preg_replace($regex, '', $child->nodeName)])) {
                        $dataXML[preg_replace($regex, '', $child->nodeName)] = [$dataXML[preg_replace($regex, '', $child->nodeName)]];
                        $groups[preg_replace($regex, '', $child->nodeName)] = 1;
                    }

                    $dataXML[preg_replace($regex, '', $child->nodeName)][] = $this->xmlToObject($child);
                }
            }
        }

        return (object) $dataXML;
    }

    public function __toString()
    {
        return json_encode($this->getResponseToObject());
    }
}
